package com.example.FirstProject;

import com.example.FirstProject.models.Role;
import com.example.FirstProject.models.User;
import com.example.FirstProject.repos.UserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;

@Controller
public class RegistrationController {
    @Autowired
    private UserRepo userRepo;

    @GetMapping("/register")
    public  String registration(){
        return "register";
    }
    @PostMapping("/register")
    public String addUser( User user, Model model){

        User userFromDb =  userRepo.findByUsername(user.getUsername());

        if (userFromDb != null){
            model.addAttribute("message", "User exist!");
            return "register";
        }
        user.setActive(true);
        user.setRoles(Collections.singleton(Role.USER));
        userRepo.save(user);
        return "redirect:/login";
    }
}
