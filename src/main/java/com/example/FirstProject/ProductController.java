package com.example.FirstProject;

import com.example.FirstProject.models.Product;
import com.example.FirstProject.models.User;
import com.example.FirstProject.repos.ProductRepo;
import com.example.FirstProject.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
public class ProductController {
    @Autowired
    private ProductRepo productRepo;
    @Autowired
    private UserRepo userRepo;
    @GetMapping("/addProduct")
    public String main(Model model){
        Iterable<Product> products = productRepo.findAll();
        model.addAttribute("products", products);
        return "addProduct";
    }

    @PostMapping("/addProduct")
    public String add(@RequestParam String title,@RequestParam String description
            ,@RequestParam String URL,@RequestParam Integer price ,Model model){
        Product product = new Product(title,description,URL,price);
        productRepo.save(product);
        Iterable<Product> products = productRepo.findAll();
        model.addAttribute("products", products);
        return "addProduct";
    }
    @PostMapping("/del")
    public	String del(@RequestParam String id){
        Product delete = productRepo.findById(id);
        productRepo.delete(delete);
        return "redirect:/addProduct";
    }
}
