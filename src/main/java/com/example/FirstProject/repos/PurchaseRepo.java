package com.example.FirstProject.repos;

import com.example.FirstProject.models.Product;
import com.example.FirstProject.models.Purchase;
import org.springframework.data.repository.CrudRepository;

public interface PurchaseRepo extends CrudRepository<Purchase, Long>{
    Purchase findById(String id);
}

