package com.example.FirstProject.repos;

import com.example.FirstProject.models.Product;

import com.example.FirstProject.models.User;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepo extends CrudRepository<Product, Long> {
    Product findById(String id);
}
