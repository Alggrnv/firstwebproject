package com.example.FirstProject.repos;


import com.example.FirstProject.models.User;

import org.springframework.data.repository.CrudRepository;


public interface UserRepo extends CrudRepository<User, Long> {
    User findByUsername(String username);
    User findById(String id);
}
