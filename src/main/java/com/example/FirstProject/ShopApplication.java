package com.example.FirstProject;


import com.example.FirstProject.models.Product;
import com.example.FirstProject.models.Purchase;
import com.example.FirstProject.models.Role;
import com.example.FirstProject.models.User;
import com.example.FirstProject.repos.ProductRepo;
import com.example.FirstProject.repos.PurchaseRepo;
import com.example.FirstProject.repos.UserRepo;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Set;



@Controller

public class ShopApplication {


    @Autowired
    private UserRepo userRepo;
    @Autowired
    private PurchaseRepo purchaseRepo;
    @Autowired
    private ProductRepo productRepo;


    @GetMapping
    public String shop(Model model){

        Iterable<Product> products = productRepo.findAll();
        model.addAttribute("products", products);

        return "shop";
    }
    @PostMapping("/buy")
    public String buy(@AuthenticationPrincipal User user, @RequestParam String id){
        Product product = productRepo.findById(id);
        Purchase purchase =new Purchase(product.title,product.price,user.getId());
        purchaseRepo.save(purchase);

        return "redirect:/";
    }


}
