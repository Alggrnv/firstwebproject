package com.example.FirstProject;

import com.example.FirstProject.models.Purchase;
import com.example.FirstProject.models.User;
import com.example.FirstProject.repos.PurchaseRepo;
import com.example.FirstProject.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;
import java.util.List;
import java.util.Set;

@Controller
public class CartController {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private PurchaseRepo purchaseRepo;

    @GetMapping("/cart")
    public String cart(@AuthenticationPrincipal User user, Model model){

        float allPrice = 0;
        Iterable<Purchase> testp = purchaseRepo.findAll();
         List<Purchase> purchases = user.getPurchases();
//        for(Purchase pur : purchases ){
//            allPrice += pur.getPrice();
//        }
        model.addAttribute("purchases", testp);
        model.addAttribute("allPrice", allPrice);
        return "cart";
    }

}
